#!/user/bin/env python
#coding:utf-8
import pymongo
import codecs
from pymongo import Connection
connection = Connection('localhost',27017)
db = connection.avmash
collection = db.avmash


def main():
    girl_info = codecs.open('result.txt','r','utf-8').readlines()
    length = len(girl_info)
    for i in range(length):
        info = {}
        if i%6 == 0:
            info['id'] = int(girl_info[i])
            info['link']  = girl_info[i+1]
            info['pic'] = girl_info[i+2]
            info['name'] = girl_info[i+3]
            info['birth'] = girl_info[i+4]
            info['score'] = 1400
            collection.insert(info)


if __name__ == '__main__':
    main()
