#!/usr/bin/env python
#-*-coding:utf-8-*-
from  pymongo import Connection
import pymongo
import random
from datetime import datetime
import web
from web.contrib.template import render_mako
RT_SITE = (                      
        ("http://v.t.sina.com.cn/share/share.php?title=%s+%s&appkey=3174430949", '新浪微博','sina'),
    ("http://shuo.douban.com/!service/share?name=%s&href=%s", "豆瓣", "douban"),
    ("http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?title=%s&url=%s","QQ空间","qzone"),
    ("http://share.renren.com/share/buttonshare.do?&title=%s&link=%s", "人人网","renren")
)   
render = render_mako(
        directories=['template'],
        input_encoding = 'utf-8',
        output_encoding = 'utf-8',
        )

connection = Connection('localhost',27017)
db = connection.avmash
collection = db.avmash
comment = db.comment

def calculate(win,lose):
    a = collection.find_one({'id':int(win)})
    b = collection.find_one({'id':int(lose)})
    scorea = a['score']
    scoreb = b['score']
    ea = 1.0/(1.0+pow(10,(scoreb-scorea)/400))
    eb = 1.0-ea
    incra = 32.0*(1-ea) 
    incrb = 32.0*(0-eb)
    collection.update({"id": int(win)}, {"$set": {"score": scorea+incra}})
    collection.update({"id": int(lose)}, {"$set": {"score": scoreb+incrb}})

    

class mash:
    def GET(self):
        data = web.input()
        if data:
            win = data['win']
            lose = data['lose']
            if win and lose:
                calculate(win,lose)
        l = random.randint(1,492)
        r = random.randint(1,492)
        resultl = collection.find_one({'id':l}) 
        resultr = collection.find_one({'id':r}) 
        values = [resultl,resultr]
        return render.index(values = values ,rt_site = RT_SITE)
    def POST(self):
        data = web.data() 

class result:
    def GET(self):
        result = comment.find().sort('time', pymongo.DESCENDING)
        avrank = collection.find().sort('score',pymongo.DESCENDING).limit(10)
        return render.result(result = result,avrank = avrank)

    def POST(self):
        data = web.input()
        com = data.get('comment')
        if com:
            comment.insert({'time':datetime.now(),'comment':com})
        raise web.seeother('/result')

